package simulador;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *
 * @author Pedro
 * Having a whole class for IPv4 parameters proved to be useful as it can
 * be used to determine its class (which is relevant to the problem at hand)
 * during time of creation.
 */
public class Ip {
    private String ipAd;
    private char classe;
    int rede;
 
   
    public Ip(String ipaddress) {
        setIpAd(ipaddress);
        setClasse(ipaddress);
    }
 
   
   
    public String getIpAd() {
        return ipAd;
    }
   
    public void setIpAd(String ipaddress) {
        String pattern = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(ipaddress);
        if(m.find()){
            ipAd = ipaddress;
        } else {
            System.out.println("Verifique se o endereço" + ipaddress + " IP de entrada estão conforme os padrões do IPv4.");
            System.exit(0);
        }
    }
 
    public char getClasse() {
        return classe;
    }
 
    public void setClasse(String ip) {
        String[] parts = ip.split(Pattern.quote("."));
        int fstByte = Integer.parseInt(parts[0]);
        if(fstByte<127){
            classe = 'A';
            rede = Integer.parseInt(parts[0]);
        }   else if (fstByte < 192) {
                classe = 'B';
                rede = Integer.parseInt(parts[0]+parts[1]);
        }   else if (fstByte < 223){
                classe = 'C';
                rede = Integer.parseInt(parts[0]+parts[1]+parts[2]);
        }   else System.out.println("Localhost ou endereço de multicasting");
    }

    public int getRede() {
        return rede;
    }
    
    public boolean sameNetwork(Ip x){
        if (x.getRede()==rede){
            return true;
        }else return false;
    }
}