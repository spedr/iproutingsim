package simulador;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pedro
 * A router object with its relevant attributes.
 * A router can have n network interfaces.
 */
public class Router {
    private String name;
    private int numPorts;
    private List<Interface> interfaces = new ArrayList<>();
    private List<RouterTable> routerTable = new ArrayList<>();
    

    public Router(String name, int numPorts) {
        this.name = name;
        this.numPorts = numPorts;
    }

    public List<RouterTable> getRouterTable() {
        return routerTable;
    }
    
    
    
    public Ip findIp(String ipAd){
        for(int i=0;i<=interfaces.size();i++){
        if(interfaces.equals(interfaces.get(i).getIp().getIpAd())){
            return interfaces.get(i).getIp();
            }
        }
        return null;
    }
    
    public List<Interface> getInterfaces() {
        return interfaces;
    }

    public boolean addInterface(Interface entry){
        if((interfaces.size()) >= (numPorts+1)){
            System.out.println("Roteador " + name + "não pode ter mais interfaces do que o seu número de portas permite");
            System.exit(0);
        }
        return interfaces.add(entry);
    }
    
    public void addToRouterTable(RouterTable entry){
        Interface e = interfaces.get(entry.getPort());
        if(entry.getNexthop().getRede()==e.getIp().getRede() || ((entry.getNexthop().getRede()==0))&&(entry.getNetdest().getRede()==e.getIp().getRede())){
            routerTable.add(entry);
            return;
        }
        System.out.println("Falta de correspondência na numeração das portas da RouterTable com as interfaces.");
        System.out.println("Verifique o roteador: " + name + "\r\nna porta " + entry.getPort() + "\r\n\r\nCom a tabela de roteamento:");
        System.out.println("Netdest " + entry.getNetdest().getIpAd() + "\r\nNexthop " + entry.getNexthop().getIpAd());
        System.out.println("\r\nInterface 1 IP: " + e.getIp().getIpAd());
        System.exit(0);
        
    }
    
    public void setInterfaces(List<Interface> interfaces) {
        this.interfaces = interfaces;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumPorts() {
        return numPorts;
    }

    public void setNumPorts(int numPorts) {
        this.numPorts = numPorts;
    }
}
