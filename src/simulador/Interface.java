package simulador;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Pedro
 * Encapsulates a simplified a router's network interface.
 */
public class Interface {
    private Ip ip;
    private String mac;
    private int mtu;

    public Interface(String ip, String mac, int mtu) {
        this.ip = new Ip(ip);
        String pattern = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(mac);
        if(m.find()){
            this.mac = mac;
        } else {
            System.out.println("Endereço MAC da interface de endereço IP " + this.ip.getIpAd() + "é inválido.");
            System.exit(0);
        }
        this.mtu = mtu;
    }
    
    

    public Ip getIp() {
        return ip;
    }

    public void setIp(Ip ip) {
        this.ip = ip;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getMtu() {
        return mtu;
    }

    public void setMtu(int mtu) {
        this.mtu = mtu;
    }
    
    
}
