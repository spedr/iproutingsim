package simulador;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 * Parses the topology file with the expected sintax.
 * Goes line by line until it finds a relevant delimiter
 * in the expected order and creates the relevant structures.
 */

public class Parser {

  public Parser(String aFileName){
    fFilePath = Paths.get(aFileName);
  }
  

  public final void processLineByLine() throws IOException {
    try (Scanner scanner =  new Scanner(fFilePath, ENCODING.name())){
        String temp = "";
            while (scanner.hasNextLine()){
                temp = scanner.nextLine();
                if(temp.equals("#NODE")){
                    temp = scanner.nextLine();
                    while(true){
                        processNode(temp);
                        temp = scanner.nextLine();
                        while(temp.isEmpty() || temp.trim().equals("") || temp.trim().equals("\r\n")) {temp = scanner.nextLine();}
                        if(temp.equals("#ROUTER")) {break;}
                        }
                    }
                if(temp.equals("#ROUTER")){
                    temp = scanner.nextLine();
                    while(true){
                        processRouter(temp);
                        temp = scanner.nextLine();
                        while(temp.isEmpty() || temp.trim().equals("") || temp.trim().equals("\r\n")) {temp = scanner.nextLine();}
                        if(temp.equals("#ROUTERTABLE")) {break;}
                        }
                    }
                if(temp.equals("#ROUTERTABLE")){
                    while(scanner.hasNextLine()){
                        while((temp.isEmpty() || temp.trim().equals("") || temp.trim().equals("\r\n")) && scanner.hasNextLine()) {temp = scanner.nextLine();}
                        if(!scanner.hasNextLine()) break;
                        temp = scanner.nextLine();
                        processRouterTable(temp);
                        }
                    }
            }
        }
    }
  

  
    protected void processNode(String aLine){
        Scanner scanner = new Scanner(aLine);
        scanner.useDelimiter(",");
        int i = 0;
        String ip = "", gateway = "", mac = "", nodeName = "", temp = "";
        int mtu = 0;
        while(scanner.hasNext()){
            temp = scanner.next();
            if(i==2) {ip = temp;}
            {if(i==0) nodeName = temp;}
            {if(i==1) mac = temp;}
            {if(i==4) gateway = temp;}
            {if(i==3) mtu = Integer.parseInt(temp);}
            i++;
        }
        Node n = new Node(ip, nodeName, mac, gateway, mtu);
        nodeList.add(n);
        addToIpList(ip);
        addToNameList(nodeName);
    }
    
    protected void processRouterTable(String aLine){
        Scanner scanner = new Scanner(aLine);
        scanner.useDelimiter(",");
        int i = 0;
        String name = "", netdest = "", nexthop = "", temp = "";
        int port = 0;
        RouterTable n = null;
        Router r = null;
        while(scanner.hasNext()){
            temp = scanner.next();
            {if(i==0) name = temp;}
            {if(i==1) netdest = temp;}
            {if(i==2) nexthop = temp;}
            if(i==3){ port = Integer.parseInt(temp);
                n = new RouterTable(netdest, nexthop, port);
                r = findRouter(name);
                //r.getRouterTable().add(n);
                r.addToRouterTable(n);
            }
            i++;
        }  
    }

    public List<Node> getNodeList() {
        return nodeList;
    }

    public List<Router> getRouterList() {
        return routerList;
    }
    
    
    
    protected void processRouter(String aLine){
        Scanner scanner = new Scanner(aLine);
        scanner.useDelimiter(",");
        int i = 0;
        String name = "", mac = "", ip = "";
        int port = 0, mtu = 0;
        String temp = "";
        Router r = null;
        
        while(scanner.hasNext()){
            temp = scanner.next();
            {if(i==0) name = temp;}
            if(i==1){
                port = Integer.parseInt(temp);
                r = new Router(name, port);
                routerList.add(r);
                addToNameList(name);
            }
            if(i>1){
                while(scanner.hasNext()){
                    
                    if(i==2) {mac = temp; i++; temp = scanner.next();}
                    if(i==3) {ip = temp; i++; temp = scanner.next();}
                    if(i==4) {
                        mtu = Integer.parseInt(temp);
                        Interface inter = new Interface(ip, mac, mtu);
                        r.addInterface(inter);
                        addToIpList(ip);
                        i = 2;
                        if(scanner.hasNext()) temp = scanner.next();
                    }
                    
                }
            }
            i++;
        }
    }
    public void addToIpList(String ip){
        for(int i = 0; i<ipList.size(); i++){
            if(ipList.get(i).getIpAd().equals(ip)){
                System.out.println("Não podem existir IPs repetidos.");
                System.out.println("Verifique o IP " + ip + " no arquivo de entrada.");
                System.exit(0);
            }
        }
        ipList.add(new Ip(ip));
    }
    
    public void addToNameList(String nome){
        for(int i = 0; i<nameList.size(); i++){
            if(nameList.get(i).equals(nome)){
                System.out.println("Não podem nomes de roteadores ou hosts repetidos.");
                System.out.println("Verifique o nome " + nome + " no arquivo de entrada.");
                System.exit(0);
            }
        }
        nameList.add(nome);
    }
    
    protected Router findRouter(String name){
        for(int i = 0; i<routerList.size(); i++) if(routerList.get(i).getName().equals(name)) return routerList.get(i);
        return null;
    }
  
    
    
    
  // PRIVATE 
  private final Path fFilePath;
  private final static Charset ENCODING = StandardCharsets.UTF_8;
  private final List<Ip> ipList = new ArrayList();
  private final List<Node> nodeList = new ArrayList();
  private final List<Router> routerList = new ArrayList();
  private final List<String> nameList = new ArrayList();

} 