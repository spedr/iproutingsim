package simulador;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
/**
 *
 * @author Pedro
 * In this context, a node is a host. Abstracts a host and its
 * relevant attributes. MAC validation is done by regex.
 */
public class Node {
    private Ip ip, gateway;
    private String nodeName, mac;
    private int mtu;

    public Node(String ip, String nodeName, String mac, String gateway, int mtu) {
        this.ip = new Ip(ip);
        this.nodeName = nodeName;
        this.mtu = mtu;
        String pattern = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(mac);
        if(m.find()){
            this.mac = mac;
        } else {
            System.out.println("Endereço MAC do host " + nodeName + " é inválido.");
            System.exit(0);
        }
        
        Ip pholder = new Ip(gateway);
        if(pholder.sameNetwork(this.ip)){
            this.gateway = pholder;
        }   else{
            System.out.println("Gateway do host "  + nodeName + " precisa ser da mesma rede de seu endereço IP.");
            System.exit(0);
        } 
    }

    public Ip getIp() {
        return ip;
    }

    public int getMtu() {
        return mtu;
    }
    
    
    
    public String getNodeName() {
        return nodeName;
    }

    public String getMac() {
        return mac;
    }

    public Ip getGateway() {
        return gateway;
    }

}