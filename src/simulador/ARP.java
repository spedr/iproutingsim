
package simulador;

/**
 *
 * @author Pedro
 * Simulates a simplified ARP Cache.
 */
public class ARP {
    private Ip ip;
    private String mac;

    public ARP(Ip ip, String mac) {
        this.ip = ip;
        this.mac = mac;
    }
    
    public Ip getIp() {
        return ip;
    }

    public String getMac() {
        return mac;
    }
    
    
}
