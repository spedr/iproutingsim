package simulador;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.io.IOException;
import java.io.File;
import java.lang.ArrayIndexOutOfBoundsException;
import java.awt.datatransfer.*;
import java.awt.Toolkit;
/**
 *
 * @author Pedro
 * Simulates classful IPv4 routing by sending a message
 * from source host A to target host B. It does that by
 * generating output that matches MsGenny diagram syntax.
 * 
 * 
 */
public class Simulador {

    private static List<Router> routers = new ArrayList();
    private static List<Node> nodes = new ArrayList();
    private static List<ARP> ARPTable = new ArrayList<>();
    private static List<String> fragments = new ArrayList<String>();
    private static Router currentRouter;
    private static Interface currentInterface, currentInterface2;
    private static String result;
    private static int ttl = 8;
    
    
    public static void main(String[] args) {
        
        String arquivo = "", origem = "", destino = "", mensagem = "";
        
        try{
            arquivo = args[0];
            origem = args[1];
            destino = args[2];
            mensagem = args[3];
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Faltam argumentos de entrada.");
            System.exit(0);
        }

        System.out.println(new File(".").getAbsolutePath());
        Parser parser = new Parser(arquivo);
        try{parser.processLineByLine();}
        catch(IOException e){ System.out.println("Arquivo não encontrado.");System.exit(0);}
        nodes = parser.getNodeList();
        routers = parser.getRouterList();

        route(origem, destino, mensagem);
        routeReply(destino, origem, mensagem);
        System.out.println(result);
        StringSelection stringSelection = new StringSelection(result);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
        
        System.out.println("Conteúdo copiado para o clipboard. Para visualizar a saída acesse: https://mscgen.js.org/");
    }
    
    public static String route(String nome1, String nome2, String message){
        fragments.add(message);
        result = "wordwraparcs=true,hscale=2.0; \r\n";
        Node n1 = findNode(nome1);
        Node n2 = findNode(nome2);
        if(n1.getIp().sameNetwork(n2.getIp())){
            arpRequestReply(n1, n2);
            mtuFragmenting(n1.getMtu());
            echoRequestFinal(n1.getNodeName(),n2.getNodeName(),n1,n2);
            received(n2, message);
            return result;
        } else{
            if(findRouter(n1.getGateway().getIpAd())){
                arpRequestReply(n1, currentInterface);
                mtuFragmenting(n1.getMtu());
                echoRequestFinal(n1.getNodeName(), currentRouter.getName(),n1,n2);
                int rededestino = n2.getIp().getRede();
                int flow = 0;
                while(flow<301){
                    for(int i = 0; i < currentRouter.getRouterTable().size() ; i++){
                        if((currentRouter.getRouterTable().get(i).getNetdest().getRede() == rededestino)&& 
                                        currentRouter.getRouterTable().get(i).getNexthop().getRede()==0){
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReply(currentInterface, n2);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinal(currentRouter.getName(), n2.getNodeName(), n1, n2);
                                    received(n2, message);
                                    return result;
                        }
                                    else if(currentRouter.getRouterTable().get(i).getNetdest().getRede()==n2.getIp().getRede() &&
                                                                        currentRouter.getRouterTable().get(i).getNexthop().getRede()!=0){
                                    Router nextHopRouter = findRouterReturn(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    Interface nextHopInterface = findInterfaceReturn(nextHopRouter, currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReplyInts(currentInterface, nextHopInterface);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinal(currentRouter.getName(), nextHopRouter.getName(), n1, n2);
                                    findRouter(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    break;
                                    
                                }  else if(currentRouter.getRouterTable().get(i).getNetdest().getRede()==0 && currentRouter.getRouterTable().get(i).getNexthop().getRede()!=0){
                                    
                                    Router nextHopRouter = findRouterReturn(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    Interface nextHopInterface = findInterfaceReturn(nextHopRouter, currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReplyInts(currentInterface, nextHopInterface);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinal(currentRouter.getName(), nextHopRouter.getName(), n1, n2);
                                    findRouter(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    break;
                                }  else if(ttl==0){
                                    result = result + currentRouter.getName() + " rbox " + currentRouter.getName() + " : Time limit exceeded.";
                                    return result;
                                } else{
                                    int aux = 0;
                                    for(int k = 0; k< currentRouter.getRouterTable().size();k++){
                                        if(currentRouter.getRouterTable().get(k).getNetdest().getRede()==0 ||
                                            currentRouter.getRouterTable().get(k).getNetdest().getRede()==n2.getIp().getRede()){
                                            aux++;
                                        }
                                    }
                                    if(aux==0){
                                        result = result + currentRouter.getName() + " rbox " + currentRouter.getName() + " : Rede inalcançável.";
                                        return result;
                                    }
                                    
                                }
                    }
            }
        }
                            
                
        
        }
        return result;
    }
    public static String routeReply(String nome1, String nome2, String message){
        fragments.clear();
        fragments.add(message);
        ttl = 8;
        Node n1 = findNode(nome1);
        Node n2 = findNode(nome2);
        //Hosts in the same network
        if(n1.getIp().sameNetwork(n2.getIp())){
            arpRequestReply(n1, n2);
            mtuFragmenting(n1.getMtu());
            echoRequestFinalReply(n1.getNodeName(),n2.getNodeName(),n1,n2);
            received(n2, message);
            return result;
        } else{
            if(findRouter(n1.getGateway().getIpAd())){
                arpRequestReply(n1, currentInterface);
                mtuFragmenting(n1.getMtu());
                echoRequestFinalReply(n1.getNodeName(), currentRouter.getName(),n1,n2);
                int rededestino = n2.getIp().getRede();
                int flow = 0;
                while(flow<301){
                    for(int i = 0; i < currentRouter.getRouterTable().size() ; i++){
                        if((currentRouter.getRouterTable().get(i).getNetdest().getRede() == rededestino)&& 
                                        currentRouter.getRouterTable().get(i).getNexthop().getRede()==0){
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReply(currentInterface, n2);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinalReply(currentRouter.getName(), n2.getNodeName(), n1, n2);
                                    received(n2, message);
                                    return result;
                        }
                                    else if(currentRouter.getRouterTable().get(i).getNetdest().getRede()==n2.getIp().getRede() &&
                                                                        currentRouter.getRouterTable().get(i).getNexthop().getRede()!=0){
                                    Router nextHopRouter = findRouterReturn(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    Interface nextHopInterface = findInterfaceReturn(nextHopRouter, currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReplyInts(currentInterface, nextHopInterface);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinalReply(currentRouter.getName(), nextHopRouter.getName(), n1, n2);
                                    findRouter(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    break;
                                    
                                }  else if(currentRouter.getRouterTable().get(i).getNetdest().getRede()==0 && currentRouter.getRouterTable().get(i).getNexthop().getRede()!=0){
                                    
                                    Router nextHopRouter = findRouterReturn(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    Interface nextHopInterface = findInterfaceReturn(nextHopRouter, currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    currentInterface = currentRouter.getInterfaces().get(currentRouter.getRouterTable().get(i).getPort());
                                    arpRequestReplyInts(currentInterface, nextHopInterface);
                                    mtuFragmenting(currentInterface.getMtu());
                                    ttl--;
                                    echoRequestFinalReply(currentRouter.getName(), nextHopRouter.getName(), n1, n2);
                                    findRouter(currentRouter.getRouterTable().get(i).getNexthop().getIpAd());
                                    break;
                                }  else if(ttl==0){
                                    result = result + currentRouter.getName() + " rbox " + currentRouter.getName() + " : Time limit exceeded.";
                                    return result;
                                } else{
                                    int aux = 0;
                                    for(int k = 0; k< currentRouter.getRouterTable().size();k++){
                                        if(currentRouter.getRouterTable().get(k).getNetdest().getRede()==0 ||
                                            currentRouter.getRouterTable().get(k).getNetdest().getRede()==n2.getIp().getRede()){
                                            aux++;
                                        }
                                    }
                                    if(aux==0){
                                        result = result + currentRouter.getName() + " rbox " + currentRouter.getName() + " : Rede inalcançável.";
                                        return result;
                                        
                                    }
                                    
                                }
                    }
            }
        }
        }
        return result;
    }    
    

    //Output manipulation
    //ARP
    public static void arpRequestReply(Node n1, Node n2){
        for(int i=0; i < ARPTable.size(); i++){
            if(ARPTable.get(i).getIp().getIpAd().equals(n2.getIp().getIpAd())){
                return;
            }
        }
        result = result + n1.getNodeName() + " box " + n1.getNodeName() + " : ARP - Who has " + n2.getIp().getIpAd() + "? Tell " + n1.getIp().getIpAd()+";\r\n";
        result = result + n2.getNodeName()+ " => " + n1.getNodeName() + " : ARP - " + n2.getIp().getIpAd() + " is at " + n2.getMac()+";\r\n";
        ARPTable.add(new ARP(n1.getIp(), n1.getMac()));
        ARPTable.add(new ARP(n2.getIp(), n2.getMac()));
    }
    
    public static void arpRequestReply(Node n1, Interface n2){
        for(int i=0; i < ARPTable.size(); i++){
            if(ARPTable.get(i).getIp().getIpAd().equals(n2.getIp().getIpAd())){
                return;
            }
        }
        result = result + n1.getNodeName() + " box " + n1.getNodeName() + " : ARP - Who has " + n2.getIp().getIpAd() + "? Tell " + n1.getIp().getIpAd()+";\r\n";
        result = result + currentRouter.getName()+ " => " + n1.getNodeName() + " : ARP - " + n2.getIp().getIpAd() + " is at " + n2.getMac()+";\r\n";
        ARPTable.add(new ARP(n1.getIp(), n1.getMac()));
        ARPTable.add(new ARP(n2.getIp(), n2.getMac()));
    }
   
    public static void arpRequestReply(Interface n1, Node n2){
        for(int i=0; i < ARPTable.size(); i++){
            if(ARPTable.get(i).getIp().getIpAd().equals(n2.getIp().getIpAd())){
                return;
            }
        }
        result = result + currentRouter.getName()+ " box " + currentRouter.getName() + " : ARP - Who has " + n2.getIp().getIpAd() + "? Tell " + n1.getIp().getIpAd()+";\r\n";
        result = result + n2.getNodeName()+ " => " + currentRouter.getName() + " : ARP - " + n2.getIp().getIpAd() + " is at " + n2.getMac()+";\r\n";
        ARPTable.add(new ARP(n1.getIp(), n1.getMac()));
        ARPTable.add(new ARP(n2.getIp(), n2.getMac()));
    }
    
    public static void arpRequestReply(Interface n1, Interface n2){
        for(int i=0; i < ARPTable.size(); i++){
            if(ARPTable.get(i).getIp().getIpAd().equals(n2.getIp().getIpAd())){
                return;
            }
        }
        Router r = findRouterReturn(n2.getIp().getIpAd()); 
        result = result + currentRouter.getName()+ " box " + currentRouter.getName() + " : ARP - Who has " + n2.getIp().getIpAd() + "? Tell " + n1.getIp().getIpAd()+";\r\n";
        result = result + currentRouter.getName() + " => " + r.getName() + " : ARP - " + n2.getIp().getIpAd() + " is at " + n2.getMac()+";\r\n";
        ARPTable.add(new ARP(n1.getIp(), n1.getMac()));
        ARPTable.add(new ARP(n2.getIp(), n2.getMac()));
    }    
    
    public static void arpRequestReplyInts(Interface n1, Interface n2){
        for(int i=0; i < ARPTable.size(); i++){
            if(ARPTable.get(i).getIp().getIpAd().equals(n2.getIp().getIpAd())){
                return;
            }
        }
        Router r = findRouterReturn(n2.getIp().getIpAd()); 
        result = result + currentRouter.getName()+ " box " + currentRouter.getName() + " : ARP - Who has " + n2.getIp().getIpAd() + "? Tell " + n1.getIp().getIpAd()+";\r\n";
        result = result + r.getName() + " => " + currentRouter.getName() + " : ARP - " + n2.getIp().getIpAd() + " is at " + n2.getMac()+";\r\n";
        ARPTable.add(new ARP(n1.getIp(), n1.getMac()));
        ARPTable.add(new ARP(n2.getIp(), n2.getMac()));
    } 
    
    //ICMP PING
    public static void echoRequestFinal(String nome1, String nome2, Node n1, Node n2){
        for(int i = 0; i < fragments.size() ; i++){
        result = result + nome1 + " => " + nome2 + " : ICMP - Echo (ping) request (src="+n1.getIp().getIpAd()+" dst="+
                                                                        n2.getIp().getIpAd()+" ttl="+ ttl + " data="+fragments.get(i)+");\r\n";            
        }
    }
    
    public static void echoRequestFinalReply(String nome1, String nome2, Node n1, Node n2){
        for(int i = 0; i < fragments.size() ; i++){
        result = result + nome1 + " => " + nome2 + " : ICMP - Echo (ping) reply (src="+n1.getIp().getIpAd()+" dst="+
                                                                        n2.getIp().getIpAd()+" ttl="+ ttl + " data="+fragments.get(i)+");\r\n";            
        }
    }
    
    //RECEIVED
    public static void received(Node n1, String message){
        result = result + n1.getNodeName() + " rbox " + n1.getNodeName() + " : Received " + message + ";\r\n";
    }
  
    //FRAGMENTATION
    public static void mtuFragmenting(int mtu){
        for(int i = 0; i < fragments.size() ; i++){
            while(fragments.get(i).length()>mtu){
                fragment(fragments.get(i), mtu);
            }
        }
    }
    
    public static void fragment(String message, int mtu){
        int positionToAdd = fragments.indexOf(message);
        fragments.remove(message);
        int index = 0, aux=0;
        while (index < message.length()) {
        fragments.add((positionToAdd+aux), message.substring(index, Math.min(index+mtu,message.length())));
        index += mtu;
        aux++;
        }
    }
    
    //SEARCH
    public static Node findNode(String nome){
        for(int i=0; i<nodes.size(); i++){
            if(nodes.get(i).getNodeName().equals(nome)) return nodes.get(i);
        }
        System.out.println("Node " + nome + " não encontrado.");
        System.exit(0);
        return null;
    }    
    
    public static boolean findRouter(String atual){
        for(int i = 0; i< routers.size(); i++){
            for(int k = 0; k< routers.get(i).getInterfaces().size(); k++){
                if(routers.get(i).getInterfaces().get(k).getIp().getIpAd().equals(atual)){
                    currentRouter = routers.get(i);
                    currentInterface = routers.get(i).getInterfaces().get(k);
                    return true;
                }
            }
        }
        System.out.println("Interface " + atual + " não encontrado.");
        System.exit(0);
        return false;
    }    
    
    public static Router findRouterReturn(String atual){
        for(int i = 0; i< routers.size(); i++){
            for(int k = 0; k< routers.get(i).getInterfaces().size(); k++){
                if(routers.get(i).getInterfaces().get(k).getIp().getIpAd().equals(atual)){
                    return routers.get(i);
                }
            }
        }
        System.out.println("Interface " + atual + " não encontrado.");
        System.exit(0);
        return null;
    }    
    
    public static Interface findInterfaceReturn(String atual){
        for(int i = 0; i< routers.size(); i++){
            for(int k = 0; k< routers.get(i).getInterfaces().size(); k++){
                if(routers.get(i).getInterfaces().get(k).getIp().getIpAd().equals(atual)){
                    return routers.get(i).getInterfaces().get(k);
                }
            }
        }
        System.out.println("Interface " + atual + " não encontrado.");
        System.exit(0);
        return null;
    }
    
    public static Interface findInterfaceReturn(Router r1, String ip){
        for(int i = 0; i < r1.getInterfaces().size();i++){
            if(r1.getInterfaces().get(i).getIp().getIpAd().equals(ip)){
                return r1.getInterfaces().get(i);
            }
        }
        System.out.println("Interface " + ip + " não encontrado.");
        System.exit(0);
        return null;
    }
    
}