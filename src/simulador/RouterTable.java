package simulador;

/**
 *
 * @author Pedro
 * Simple abstraction of a router table.
 * In this model, the specified port is equivalent
 * to the interface number.
 */
public class RouterTable {
    private Ip netdest, nexthop;
    private int port;

    public RouterTable(String netdest, String nexthop, int port) {
        this.netdest = new Ip(netdest);
        this.nexthop = new Ip (nexthop);
        this.port = port;
    }

    public Ip getNetdest() {
        return netdest;
    }

    public Ip getNexthop() {
        return nexthop;
    }

    public int getPort() {
        return port;
    }
    
}
