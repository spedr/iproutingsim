# README #

Generates IP datagram routing MsGenny (https://mscgen.js.org/) diagrams based on a target topology specified by a text file.
Abstracts elements such as fragmenting purely based on MTU.

Program can be ran as 
```
#!java

java -jar Simulador.jar <topology.txt> <source host> <target host> <message>

```

Network topology notation is as following:

#NODE
<node name>, <MAC address>, <gateway IPv4 address>, <MTU value>, <node IPv4 address>
...
#ROUTER
<router name>, <number of network interfaces>, <i1 interface MAC address>, <i1 interface IPv4 address>, ..., <MTU value>
...
#ROUTERTABLE
<router name>, <target network>, <nexthop>, <port number>